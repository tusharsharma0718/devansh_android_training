package com.devansh.android.training.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devansh.android.training.R;
import com.devansh.android.training.UserDataModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserDetailsAdapter extends RecyclerView.Adapter<UserDetailsAdapter.UserDataViewHolder> {

    private final List<UserDataModel> data;
    private Context context;

    public UserDetailsAdapter(List<UserDataModel> data, Context context)
    {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public UserDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_item_layout, parent, false);
        return new UserDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserDataViewHolder holder, int position) {
        UserDataModel userDataModel = data.get(position);
        holder.adName.setText(userDataModel.getName());
        holder.adEmail.setText(userDataModel.getEmail());
        holder.adDOB.setText(userDataModel.getDateOfBirth());
        holder.adMobile.setText(userDataModel.getMobile());
        holder.adGender.setText(userDataModel.getGender());
        if(userDataModel.getImageUri().isEmpty()) {
            holder.adImage.setImageResource(R.drawable.profile_image);
        }
        else {
            Uri uri = Uri.parse(userDataModel.getImageUri());
            holder.adImage.setImageURI(uri);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class UserDataViewHolder extends RecyclerView.ViewHolder{
        private TextView adName, adEmail, adDOB, adMobile, adGender;
        private ImageView adImage;
        public UserDataViewHolder(@NonNull View itemView) {
            super(itemView);
            adImage = itemView.findViewById(R.id.l_image);
            adName = itemView.findViewById(R.id.l_name);
            adEmail = itemView.findViewById(R.id.l_email);
            adDOB = itemView.findViewById(R.id.l_dob);
            adMobile = itemView.findViewById(R.id.l_mobile);
            adGender = itemView.findViewById(R.id.l_gender);
        }
    }
}
