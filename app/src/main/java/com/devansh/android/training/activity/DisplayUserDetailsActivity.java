package com.devansh.android.training.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.devansh.android.training.R;
import com.devansh.android.training.UserDataModel;
import com.devansh.android.training.adapter.UserDetailsAdapter;
import com.devansh.android.training.database.DatabaseManager;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class DisplayUserDetailsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    UserDetailsAdapter userDetailsAdapter;
    TextView tv_emptyDatabase;
    ImageView img_back;
    TextView tv_title;
    DatabaseManager databaseManager;
    ArrayList<UserDataModel> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        settingLayoutRef();
        setLayoutAdapter();
        actionListeners();

    }

    private void actionListeners() {
        img_back.setOnClickListener(v -> {
            finish();
        });
    }

    private void setLayoutAdapter() {

        if(list.isEmpty()) {
            recyclerView.setVisibility(View.INVISIBLE);
            tv_emptyDatabase.setVisibility(View.VISIBLE);
        } else {
            userDetailsAdapter = new UserDetailsAdapter(list, this);
            recyclerView.setAdapter(userDetailsAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    private void settingLayoutRef()
    {
        img_back = findViewById(R.id.toolbar_btn_back);
        img_back.setVisibility(View.VISIBLE);
        tv_title = findViewById(R.id.toolbar_title);
        tv_title.setText(R.string.all_user_data_title);

        recyclerView = findViewById(R.id.rv_user_data);
        tv_emptyDatabase = findViewById(R.id.tv_empty_database);
        databaseManager = new DatabaseManager(this);
        list = (ArrayList<UserDataModel>) databaseManager.getUserData();
    }

}
