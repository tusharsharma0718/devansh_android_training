package com.devansh.android.training;

import java.io.Serializable;

public class UserDataModel implements Serializable {

    private String name;
    private String email;
    private String password;
    private String dateOfBirth;
    private String imageUri;
    private String gender;

    private String mobile;

    UserDataModel() {
        imageUri = "";
    }

    public UserDataModel(String name, String email, String password, String dateOfBirth, String imageUri, String gender, String mobile) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.dateOfBirth = dateOfBirth;
        this.imageUri = imageUri;
        this.gender = gender;
        this.mobile = mobile;

    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}

