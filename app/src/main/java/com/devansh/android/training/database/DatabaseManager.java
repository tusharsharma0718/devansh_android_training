package com.devansh.android.training.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.devansh.android.training.Constants;
import com.devansh.android.training.UserDataModel;

import java.util.ArrayList;
import java.util.List;


public class DatabaseManager extends SQLiteOpenHelper {

    public DatabaseManager(Context context) {
        super(context, Constants.DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + Constants.TABLE_NAME + "( " + Constants.COL_1_NAME + " TEXT, " + Constants.COL_2_EMAIL + " TEXT PRIMARY KEY, "+
                Constants.COL_3_PASSWORD + " TEXT, "+ Constants.COL_4_DOB + " TEXT, "+ Constants.COL_5_MOBILE + " TEXT, "+ Constants.COL_6_GENDER + " TEXT, " + Constants.COL_7_IMAGE + " TEXT);");
    }

    public void clearDataBase(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + Constants.TABLE_NAME +";");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String name, String email, String pass, String dob, String mobile, String gender, String image)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.COL_1_NAME,name);
        contentValues.put(Constants.COL_2_EMAIL,email);
        contentValues.put(Constants.COL_3_PASSWORD,pass);
        contentValues.put(Constants.COL_4_DOB,dob);
        contentValues.put(Constants.COL_5_MOBILE,mobile);
        contentValues.put(Constants.COL_6_GENDER,gender);
        if (image.equals("null")) {
            image = "";
        }
        contentValues.put(Constants.COL_7_IMAGE, image);
        long result = db.insert(Constants.TABLE_NAME,null,contentValues);
        return result != -1;
    }

    public List<UserDataModel> getUserData() {
        List<UserDataModel> userDatumModels =new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from " + Constants.TABLE_NAME , null);
        if (cursor.moveToFirst()){
            do {
                String name = cursor.getString(0);
                String email = cursor.getString(1);
                String password = cursor.getString(2);
                String dob = cursor.getString(3);
                String mobile = cursor.getString(4);
                String gender = cursor.getString(5);
                String image = cursor.getString(6);
                UserDataModel newUserDataModel = new UserDataModel(name, email, password, dob, image, gender, mobile);
                userDatumModels.add(newUserDataModel);
            }while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return userDatumModels;
    }

}
