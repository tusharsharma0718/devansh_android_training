package com.devansh.android.training.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.devansh.android.training.R;
import com.devansh.android.training.UserDataModel;
import com.devansh.android.training.database.DatabaseManager;

import java.util.ArrayList;
import java.util.Calendar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import static com.devansh.android.training.R.id.name;

public class UserInputActivity extends AppCompatActivity implements View.OnClickListener {

    private final static int PICK_IMAGE = 1;
    private static final int STORAGE_PERMISSION_CODE = 101;
    private EditText et_Name;
    private EditText et_Email;
    private EditText et_Password;
    private EditText et_DateOfBirth;
    private EditText et_mobile;
    private ImageView iv_profileImage;
    private RadioGroup rg_gender;
    private Calendar calendar = Calendar.getInstance();
    private Uri uri_image;
    private DatabaseManager databaseManager = new DatabaseManager(this);
    private String nameString, emailString, passwordString, genderString="", dateOfBirthString = "", mobileString;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_input_data);
        settingLayoutRef();
    }

    private void settingLayoutRef() {
        databaseManager = new DatabaseManager(this);
        et_Name = findViewById(name);
        et_Email = findViewById(R.id.email);
        et_Password = findViewById(R.id.password);
        et_DateOfBirth = findViewById(R.id.date_of_birth);
        et_mobile = findViewById(R.id.mobile);
        iv_profileImage = findViewById(R.id.profile_image);
        rg_gender = findViewById(R.id.gender);
        TextView tv_title = findViewById(R.id.toolbar_title);
        tv_title.setText(R.string.user_data_screen_title);
        RadioButton rb_male = findViewById(R.id.male);
        RadioButton rb_female = findViewById(R.id.female);
        Button btn_login = findViewById(R.id.submit_button);
        Button btn_view_all = findViewById(R.id.view_button);
        Button btn_clear_db = findViewById(R.id.clear_db);
        rb_male.setOnClickListener(this);
        rb_female.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        btn_view_all.setOnClickListener(this);
        btn_clear_db.setOnClickListener(this);
        et_DateOfBirth.setOnClickListener(this);
        iv_profileImage.setOnClickListener(this);
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    private void clearFields(){
        et_Email.setText("");
        et_Name.setText("");
        et_Password.setText("");
        et_DateOfBirth.setText("");
        et_mobile.setText("");
        rg_gender.clearCheck();
        iv_profileImage.setImageDrawable(getDrawable(R.drawable.profile_image));
    }

    private boolean DataInsertionInDataBase() {
        return databaseManager.insertData(nameString, emailString, passwordString, dateOfBirthString, mobileString, genderString, uri_image + "");
    }

    public void onClick(View v) {
        if (v.getId() == R.id.profile_image) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent. ACTION_OPEN_DOCUMENT);
            startActivityForResult(Intent.createChooser(intent,"Select Picture"),PICK_IMAGE);
        } else if (v.getId() == R.id.male) {
            genderString = "Male";
        } else if (v.getId() == R.id.female) {
            genderString = "Female";
        } else if (v.getId() == R.id.date_of_birth) {
            DatePickerDialog.OnDateSetListener datePickerDialog = (view, year, month, dayOfMonth) -> {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dateOfBirthString = calendar.get(Calendar.DAY_OF_MONTH) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.YEAR);
                et_DateOfBirth.setText(dateOfBirthString);
            };
            new DatePickerDialog(UserInputActivity.this, datePickerDialog, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
        } else if (v.getId() == R.id.clear_db) {
            ArrayList<UserDataModel> data = (ArrayList<UserDataModel>) databaseManager.getUserData();
            if ( data.isEmpty() ) {
                Toast.makeText(getApplicationContext(), R.string.already_empty_db, Toast.LENGTH_SHORT).show();
            } else {
                databaseManager.clearDataBase();
                Toast.makeText(getApplicationContext(), R.string.clearing_db, Toast.LENGTH_SHORT).show();
            }
        } else if (v.getId() == R.id.view_button) {
            Intent intent = new Intent(UserInputActivity.this, DisplayUserDetailsActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.submit_button) {
            validation();
        }
    }

    private void validation() {
        nameString = et_Name.getText().toString();
        emailString = et_Email.getText().toString();
        passwordString = et_Password.getText().toString();
        mobileString = et_mobile.getText().toString();
        if (!(nameString.isEmpty())) {
            if (!(emailString.isEmpty())) {
                if (Patterns.EMAIL_ADDRESS.matcher(emailString).matches()) {
                    if (!(passwordString.isEmpty())) {
                        if (passwordString.length() >= 8) {
                            if (!(dateOfBirthString.isEmpty())) {
                                if (!(mobileString.isEmpty())) {
                                    if (mobileString.length() == 10) {
                                        if (!(genderString.isEmpty())) {
                                            if (DataInsertionInDataBase()) {
                                                Toast.makeText(getApplicationContext(), R.string.data_inserted, Toast.LENGTH_SHORT).show();
                                                clearFields();
                                            } else {
                                                Toast.makeText(getApplicationContext(), R.string.duplicate_email, Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), R.string.empty_gender, Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), R.string.invalid_mobile, Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), R.string.empty_mobile, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.empty_dob, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.invalid_password, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.empty_password , Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.invalid_email, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), R.string.empty_email, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), R.string.empty_name, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            if ( data != null) {
                uri_image = data.getData();
                if (uri_image != null)
                    iv_profileImage.setImageURI(uri_image);
            }
        }
    }
}