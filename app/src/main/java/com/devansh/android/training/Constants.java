package com.devansh.android.training;

public class Constants {

    public static final String DATABASE_NAME = "USERS";
    public static final String TABLE_NAME = "USER_DETAILS";
    public static final String COL_1_NAME = "NAME";
    public static final String COL_2_EMAIL = "EMAIL";
    public static final String COL_3_PASSWORD = "PASSWORD";
    public static final String COL_4_DOB = "DOB";
    public static final String COL_5_MOBILE = "MOBILE";
    public static final String COL_6_GENDER = "GENDER";
    public static final String COL_7_IMAGE = "IMAGE";

}
